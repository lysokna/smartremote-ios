//
//  SRFirebaseManager.m
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import "SRFirebaseManager.h"

@interface SRFirebaseManager ()

@property (nonatomic, strong) Firebase *rootRef;

@end

@implementation SRFirebaseManager

+ (instancetype)sharedManager {
  static dispatch_once_t onceToken;
  static SRFirebaseManager *manager;
  dispatch_once(&onceToken, ^{
    manager = [[self alloc] init];
  });
  return manager;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    self.rootRef = [[Firebase alloc] initWithUrl:@"https://smart-remote.firebaseio.com"];
  }
  return self;
}

- (void)sendEvent:(NSString *)eventname
        toService:(NSString *)serviceName {
  NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
  Firebase *eventRef = [self.rootRef childByAppendingPath:serviceName];
  [eventRef setValue:[NSString stringWithFormat:@"%f-%@",timeInterval, eventname]];
}

- (void)sendPresentationEvent:(NSString *)event {
  [self sendEvent:event toService:@"presentation"];
}

- (void)sendVideoEvent:(NSString *)event {
  NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
  Firebase *eventRef = [self.rootRef childByAppendingPath:@"video"];
  NSDictionary *dict = @{@"control":[NSString stringWithFormat:@"%f-%@",timeInterval, event]};
  [eventRef updateChildValues:dict];
}

- (void)sendVideoVolumeEventWithValue:(NSNumber *)value {
  Firebase *eventRef = [self.rootRef childByAppendingPath:@"video"];
  NSDictionary *dict = @{@"volume":value};
  [eventRef updateChildValues:dict];
}

- (void)sendGameEvent:(NSString *)event {
  [self sendEvent:event toService:@"game"];
}

- (void)sendMenuEvent:(NSString *)event {
  [self sendEvent:event toService:@"menu"];
}

@end
