//
//  SRFirebaseManager.h
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase.h>

@interface SRFirebaseManager : NSObject

+ (instancetype)sharedManager;

- (void)sendPresentationEvent:(NSString *)event;

- (void)sendVideoEvent:(NSString *)event;

- (void)sendVideoVolumeEventWithValue:(NSNumber *)value;

- (void)sendGameEvent:(NSString *)event;

- (void)sendMenuEvent:(NSString *)event;

@end
