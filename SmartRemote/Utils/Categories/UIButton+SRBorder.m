//
//  UIButton+SRBorder.m
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import "UIButton+SRBorder.h"

@implementation UIButton (SRBorder)

- (void)setupBorder {
  self.layer.cornerRadius = 3;
  self.layer.borderColor = [UIColor whiteColor].CGColor;
  self.layer.borderWidth = 3;
}

@end
