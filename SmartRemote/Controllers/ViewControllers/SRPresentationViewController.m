//
//  SRPresentationViewController.m
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import "SRPresentationViewController.h"
#import <CoreMotion/CoreMotion.h>

@interface SRPresentationViewController ()

@property (weak, nonatomic) IBOutlet UIView *swipeView;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic, strong) CMMotionManager *motionManager;
@end

@implementation SRPresentationViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.motionManager = [[CMMotionManager alloc] init];
}

- (void)dealloc {
  self.motionManager = nil;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (IBAction)sensorSwitchAction:(UISwitch *)sender {
  
  if (sender.on) {
    if (self.motionManager.deviceMotionAvailable) {
      self.motionManager.deviceMotionUpdateInterval = 0.05f;
      [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue mainQueue]
                                              withHandler:^(CMDeviceMotion *data, NSError *error) {
                                                NSLog(@"%f",data.userAcceleration.x);
                                                if (data.userAcceleration.x < -2.0f) {
                                                  NSLog(@"shake left");
                                                  [NSObject cancelPreviousPerformRequestsWithTarget:self];
                                                  
                                                  [self performSelector:@selector(backButtonAction:) withObject:nil afterDelay:0.1f];
                                                } else if (data.userAcceleration.x > 2.0f) {
                                                  NSLog(@"shake right");
                                                  [NSObject cancelPreviousPerformRequestsWithTarget:self];
                                                  
                                                  [self performSelector:@selector(nextButtonAction:) withObject:nil afterDelay:0.1f];
                                                }
                                              }];
    } else {
      [self.motionManager stopDeviceMotionUpdates];
    }
  }
}
- (IBAction)closeButtomAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:^{
    [[SRFirebaseManager sharedManager] sendMenuEvent:@"home"];
  }];
}

- (IBAction)backButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendPresentationEvent:@"moveBackward"];
}

- (IBAction)nextButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendPresentationEvent:@"moveForward"];
}

- (IBAction)swipeRightAction:(id)sender {
  self.backButton.hidden = NO;
  [[SRFirebaseManager sharedManager] sendPresentationEvent:@"moveBackward"];
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    self.backButton.hidden = YES;
  });
}
- (IBAction)swipeLeftAction:(id)sender {
  self.nextButton.hidden = NO;
  [[SRFirebaseManager sharedManager] sendPresentationEvent:@"moveForward"];
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    self.nextButton.hidden = YES;
  });
}
@end
