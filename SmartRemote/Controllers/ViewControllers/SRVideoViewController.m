//
//  SRVideoViewController.m
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import "SRVideoViewController.h"

@interface SRVideoViewController ()

@end

@implementation SRVideoViewController

- (void)viewDidLoad {
  [super viewDidLoad];
}
- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}
- (IBAction)closeButtomAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:^{
    [[SRFirebaseManager sharedManager] sendMenuEvent:@"home"];
  }];
}

- (IBAction)playButtonAction:(UIButton *)sender {
  
  if (sender.selected) {
    [[SRFirebaseManager sharedManager] sendVideoEvent:@"pause"];
  } else {
    [[SRFirebaseManager sharedManager] sendVideoEvent:@"play"];
  }
  sender.selected = !sender.selected;
  
}
- (IBAction)nextButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendVideoEvent:@"next"];
}
- (IBAction)previousButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendVideoEvent:@"back"];
}
- (IBAction)volumeSliderChange:(UISlider *)sender {
  [[SRFirebaseManager sharedManager] sendVideoVolumeEventWithValue:@(sender.value)];
}

@end
