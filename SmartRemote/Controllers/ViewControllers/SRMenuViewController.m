//
//  ViewController.m
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import "SRMenuViewController.h"
#import "UIButton+SRBorder.h"

@interface SRMenuViewController ()

@property (weak, nonatomic) IBOutlet UIButton *presentationButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *gameButton;
@end

@implementation SRMenuViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.presentationButton setupBorder];
  [self.videoButton setupBorder];
  [self.gameButton setupBorder];
  [[SRFirebaseManager sharedManager] sendMenuEvent:@"home"];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  [[SRFirebaseManager sharedManager] sendMenuEvent:segue.identifier];
}


@end
