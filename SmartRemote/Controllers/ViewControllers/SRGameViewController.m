//
//  SRGameViewController.m
//  SmartRemote
//
//  Created by Sokna Ly on 5/4/16.
//  Copyright © 2016 Sokna Ly. All rights reserved.
//

#import "SRGameViewController.h"

@interface SRGameViewController ()

@end

@implementation SRGameViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}
- (IBAction)closeButtomAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:^{
    [[SRFirebaseManager sharedManager] sendMenuEvent:@"home"];
  }];
}

- (IBAction)upButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendGameEvent:@"up"];
}
- (IBAction)downButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendGameEvent:@"down"];
}
- (IBAction)leftButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendGameEvent:@"left"];
}
- (IBAction)rightButtonAction:(id)sender {
  [[SRFirebaseManager sharedManager] sendGameEvent:@"right"];
}
@end
